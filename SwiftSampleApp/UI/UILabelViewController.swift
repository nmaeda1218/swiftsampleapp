//
//  UILabelViewController.swift
//  SwiftSampleApp
//
//  Created by nobukazu on 2019/08/14.
//  Copyright © 2019 nmaeda1218. All rights reserved.
//

import UIKit

@objc(UILabelViewController)

class UILabelViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.view.backgroundColor = UIColor.red

        // Do any additional setup after loading the view.
    }

    
    @IBAction func didTouchCloseButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
