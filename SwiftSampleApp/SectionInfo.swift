//
//  SectionInfo.swift
//  SwiftSampleApp
//
//  Created by nobukazu on 2019/08/13.
//  Copyright © 2019 nmaeda1218. All rights reserved.
//

import Foundation


class SectionInfo: NSObject {
    var title: String = ""
    var items: [String] = []
}

extension SectionInfo {
    
    static func SectionInfos() -> [SectionInfo]  {
        
        
        var infos: [SectionInfo] = []
        
        
        
        
        
        
        
        var info = SectionInfo()
        info.title = "UI"
        info.items = ["UILabel"]
        infos.append(info)
        
        info = SectionInfo()
        info.title = "Function"
        info.items = ["SpeechIO", "SpeechSynthesizer"]
        infos.append(info)
        
        return infos
        
        
        
        
        
        
        
        
        
//        return [
//            SectionInfo(title: "UILabel", items: ["北海道"]),
//            SectionInfo(title: "UIView", items: ["青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県"]),
//            SectionInfo(title: "関東地方", items: ["茨城県", "栃木県", "群馬県", "埼玉県", "千葉県", "東京都", "神奈川県"]),
//            SectionInfo(title: "中部地方", items: ["新潟県", "富山県", "石川県", "福井県", "岐阜県", "長野県", "山梨県", "静岡県", "愛知県"]),
//            SectionInfo(title: "近畿地方", items: ["大阪府" , "京都府", "兵庫県", "奈良県", "三重県", "滋賀県", "和歌山県"]),
//            SectionInfo(title: "中国地方", items: ["鳥取県", "島根県", "岡山県", "広島県", "山口県"]),
//            SectionInfo(title: "四国地方", items: ["徳島県", "香川県", "愛媛県", "高知県"]),
//            SectionInfo(title: "九州地方", items: ["福岡県", "佐賀県", "長崎県", "大分県", "熊本県", "宮崎県", "鹿児島県", "沖縄県"]),
//        ]
    }
}
