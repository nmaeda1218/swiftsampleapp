//
//  SpeechSynthesizerViewController.swift
//  SwiftSampleApp
//
//  Created by nobukazu on 2019/08/16.
//  Copyright © 2019 nmaeda1218. All rights reserved.
//

import UIKit
import AVFoundation

@objc(SpeechSynthesizerViewController)

class SpeechSynthesizerViewController: UIViewController {
    
    private let synthesizer = AVSpeechSynthesizer()
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        let singleTapGesture = UITapGestureRecognizer(target: self,
                                                      action: #selector(SpeechSynthesizerViewController.onTap(gesture:)))
        singleTapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(singleTapGesture)
        
        
        textView.text = UIPasteboard.general.string
    }


    @objc fileprivate func onTap(gesture: UITapGestureRecognizer) {
        if gesture.numberOfTapsRequired == 1 {
            // シングルタップ時の動作
            
            self.view.endEditing(true)
        }
        else if gesture.numberOfTapsRequired == 2 {
            // ダブルタップ時の動作
        }
    }
    
    func speech() {
        
        // AudioSessionのカテゴリーをセットしているのは、この後に出てくるSFSpeechRecognizerの処理との兼ね合いです。
        // これをセットしないと、音声認識を行った後にスピーカーから声が再生されません。
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        
        let utterance = AVSpeechUtterance(string: textView.text!)
        utterance.voice = AVSpeechSynthesisVoice(language: "ja-JP") // 言語
        utterance.rate = 0.5; // 読み上げ速度
        utterance.pitchMultiplier = 1.2; // 読み上げる声のピッチ
        utterance.preUtteranceDelay = 0.2; // 読み上げるまでのため
        self.synthesizer.speak(utterance)
    }
    
    /// Closeボタン押下時に呼ばれる
    ///
    /// - Parameter sender: 送信元インスタンンス
    @IBAction func didTouchCloseButton(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }

    /// Speechボタン押下時に呼ばれる
    ///
    /// - Parameter sender: 送信元インスタンンス
    @IBAction func didTouchSeechButton(_ sender: Any) {
        speech()
    }
}
