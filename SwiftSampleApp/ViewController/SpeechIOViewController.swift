//
//  SpeechIOViewController.swift
//  SwiftSampleApp
//
//  Created by nobukazu on 2019/08/15.
//  Copyright © 2019 nmaeda1218. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

@objc(SpeechIOViewController)

class SpeechIOViewController: UIViewController {
    
    private let synthesizer = AVSpeechSynthesizer()

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stopButton: UIButton!
    
    @IBOutlet weak var startButton: UIButton!
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ja-JP"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestRecognizerAuthorization()

        try? start()
    }
    
    private func requestRecognizerAuthorization() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
        }
    }
    
    private func start() throws {
        
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }

        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(.record, mode: .measurement, options: [])
        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        
        let recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        self.recognitionRequest = recognitionRequest
        recognitionRequest.shouldReportPartialResults = true
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { [weak self] (result, error) in
            guard let `self` = self else { return }
            
            var isFinal = false
            
            if let result = result {
                print(result.bestTranscription.formattedString)
                self.textView.text = result.bestTranscription.formattedString
                isFinal = result.isFinal
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                self.audioEngine.inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
            }
        }
        
        let recordingFormat = audioEngine.inputNode.outputFormat(forBus: 0)
        audioEngine.inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        try? audioEngine.start()
        
        titleLabel.text = "SpeechRecognizing..."
        titleLabel.textColor = UIColor.red
        startButton.isEnabled = false
    }
    
    /// 音声認識を終了する
    private func stop() {
        audioEngine.stop()
        recognitionRequest?.endAudio()
        
        titleLabel.text = "Waiting..."
        titleLabel.textColor = UIColor.blue
        startButton.isEnabled = true
    }

    /// Closeボタン押下時に呼ばれる
    ///
    /// - Parameter sender: 送信元インスタンンス
    @IBAction func didTouchCloseButton(_ sender: Any) {
        stop()
        UIPasteboard.general.string = textView.text!
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Startボタン押下時に呼ばれる
    ///
    /// - Parameter sender: 送信元インスタンンス
    @IBAction func didTouchStartButton(_ sender: Any) {
        try? start()
    }

    /// Stopボタン押下時に呼ばれる
    ///
    /// - Parameter sender: 送信元インスタンンス
    @IBAction func didTouchStopButton(_ sender: Any) {
        stop()
    }
    
    /// Speechボタン押下時に呼ばれる
    ///
    /// - Parameter sender: 送信元インスタンンス
    @IBAction func didTouchSeechButton(_ sender: Any) {
        speech()
    }
    
    /// テキスト内容を読み上げる
    func speech() {
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        
        let utterance = AVSpeechUtterance(string: textView.text!)
        utterance.voice = AVSpeechSynthesisVoice(language: "ja-JP") // 言語
        utterance.rate = 0.5; // 読み上げ速度
        utterance.pitchMultiplier = 1.2; // 読み上げる声のピッチ
        utterance.preUtteranceDelay = 0.2; // 読み上げるまでのため
        self.synthesizer.speak(utterance)
    }
}
