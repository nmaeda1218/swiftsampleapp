//
//  UIViewController+Extension.swift
//  SwiftSampleApp
//
//  Created by nobukazu on 2019/08/14.
//  Copyright © 2019 nmaeda1218. All rights reserved.
//

import UIKit
import Foundation

extension UIViewController {

    /**
     クラス名からViewController系インスタンス（xib）を返す
     - parameter className:クラス名
     - returns:ViewController系インスタンス
     */
    static func xibViewController(className: String) -> UIViewController {
        let obj = NSClassFromString(className) as! UIViewController.Type
        let vc = obj.init(nibName: className, bundle: nil)
        return vc
    }

}
